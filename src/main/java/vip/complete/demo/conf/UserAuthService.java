package vip.complete.demo.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import vip.complete.demo.user.UserModel;
import vip.complete.demo.user.UserService;

import java.util.Optional;

@Service
public class UserAuthService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<UserModel> userOpt = userService.getUserByUsername(s);
        userOpt.orElseThrow(()-> new UsernameNotFoundException("User Not Found"));
        return new UserDetailsConf(userOpt.get());
    }

}
