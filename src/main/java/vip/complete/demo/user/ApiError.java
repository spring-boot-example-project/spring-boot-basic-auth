package vip.complete.demo.user;

import lombok.Data;

import java.util.Date;
import java.util.Map;

@Data
public class ApiError {

    private int errorCode;
    private String message;
    private String path;
    private long timestamp=new Date().getTime();
    private Map<String,String> validationMap;

    public ApiError(int errorCode, String message,String path) {
        this.errorCode=errorCode;
        this.message =message;
        this.path = path;
    }

}
