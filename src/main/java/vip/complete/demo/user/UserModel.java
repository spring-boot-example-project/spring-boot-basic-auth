package vip.complete.demo.user;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
public class UserModel {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Length(min = 3, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String username;

    @Size(min = 3, max = 16)
    @Column(unique = true, nullable = false,length = 255)
    private String password;


}
